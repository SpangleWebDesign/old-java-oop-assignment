
package CQUPControl;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.ObjectOutputStream;
import java.io.Writer;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import java.text.SimpleDateFormat;
/** 
 * Programmer: Mitchell Boland / S0249789
 * File: DataEntryForm.java
 * Date: 06/04/2016
 * Purpose: COIT11134 Object Oriented Programming (T1,2016)
 * The DataEntryForm is used to extend a JFrame. This JFrame will take user 
 * input and using a submit button, store all the information in separate
 * classes. The job object will then take all this information and display it
 * in a text area on the bottom of this form.
 * It has the ability to go back to the title page and wipe all current data
 * entered as well as display the Job object info in a text area.
 */
public class DataEntryForm extends javax.swing.JFrame {
    FrameManager frameManager = new FrameManager();
    
    // Set these to final so they can not be changed during run time.
    private final double GENERAL_PEST_COST = 190;
    private final double TERMITE_INSPECT_COST = 200;
    private final double TERMITE_FILL_COST = 350;
    private final int ROOM_COST = 10;
    private final int HALL_COST = 15;
    private final int NON_CHARGE_ROOM = 0;
    private double counter = 0;
    private  String stringDate = "";
    private  DateManage dateManage;
    private double roomsAndCost;
    /**
     * Creates new form DataEntryForm
     */
    
    public DataEntryForm() {
        initComponents();
        // Do not want the user to be able to resize the application
        setResizable(false);
              
    }

    /**
     * This method is called from within the constructor to initialize the form.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroupSelectContractor = new javax.swing.ButtonGroup();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jPanelJobIdAndDate = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jLabelDate = new javax.swing.JLabel();
        jTextFieldJobID = new javax.swing.JTextField();
        jDateChooser_Job_Date = new com.toedter.calendar.JDateChooser();
        jPanel2 = new javax.swing.JPanel();
        jTextFieldAddressNumber = new javax.swing.JTextField();
        jTextFieldStreet = new javax.swing.JTextField();
        jTextFieldPostCode = new javax.swing.JTextField();
        jTextFieldSuburb = new javax.swing.JTextField();
        jButtonSubmit = new javax.swing.JButton();
        jPanel_Job_Types_Holder = new javax.swing.JPanel();
        jCheckBoxGeneralPestControl = new javax.swing.JCheckBox();
        jCheckBoxTermiteInspection = new javax.swing.JCheckBox();
        jCheckBoxTermiteControlFill = new javax.swing.JCheckBox();
        jSpinner_Rooms = new javax.swing.JSpinner();
        jSpinner_Halls = new javax.swing.JSpinner();
        jSpinner_Non_Charge_Rooms = new javax.swing.JSpinner();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jPanel_Contractor_Holder = new javax.swing.JPanel();
        jRadioButtonContractor1 = new javax.swing.JRadioButton();
        jRadioButtonContractor2 = new javax.swing.JRadioButton();
        jRadioButtonContractor3 = new javax.swing.JRadioButton();
        jButtonClear = new javax.swing.JButton();
        jButtonClear1 = new javax.swing.JButton();
        jPanel_TableHolder = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTable_Alll_Job_Details = new javax.swing.JTable();
        jButton_Clear_Table = new javax.swing.JButton();
        jButton_Load_Past_Jobs = new javax.swing.JButton();
        jButton1 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Data Entry Form");
        setBackground(new java.awt.Color(204, 255, 204));

        jPanel1.setBackground(new java.awt.Color(119, 158, 203));
        jPanel1.setBorder(javax.swing.BorderFactory.createEtchedBorder(java.awt.Color.black, java.awt.Color.black));

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("Job Card Index");

        jPanelJobIdAndDate.setBackground(new java.awt.Color(119, 158, 203));
        jPanelJobIdAndDate.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Job ID and Date", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 11))); // NOI18N

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel2.setText("Job ID");

        jLabelDate.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabelDate.setText("Date");

        jTextFieldJobID.setText("Number");
        jTextFieldJobID.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                jTextFieldJobIDFocusGained(evt);
            }
        });

        jDateChooser_Job_Date.setDateFormatString("d MMM, yyyy");

        javax.swing.GroupLayout jPanelJobIdAndDateLayout = new javax.swing.GroupLayout(jPanelJobIdAndDate);
        jPanelJobIdAndDate.setLayout(jPanelJobIdAndDateLayout);
        jPanelJobIdAndDateLayout.setHorizontalGroup(
            jPanelJobIdAndDateLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelJobIdAndDateLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanelJobIdAndDateLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanelJobIdAndDateLayout.createSequentialGroup()
                        .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 73, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabelDate)
                        .addGap(75, 75, 75))
                    .addGroup(jPanelJobIdAndDateLayout.createSequentialGroup()
                        .addComponent(jTextFieldJobID, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jDateChooser_Job_Date, javax.swing.GroupLayout.PREFERRED_SIZE, 157, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap())))
        );
        jPanelJobIdAndDateLayout.setVerticalGroup(
            jPanelJobIdAndDateLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelJobIdAndDateLayout.createSequentialGroup()
                .addGroup(jPanelJobIdAndDateLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabelDate))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanelJobIdAndDateLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jDateChooser_Job_Date, javax.swing.GroupLayout.DEFAULT_SIZE, 23, Short.MAX_VALUE)
                    .addComponent(jTextFieldJobID))
                .addContainerGap())
        );

        jPanel2.setBackground(new java.awt.Color(119, 158, 203));
        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Address", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 11))); // NOI18N

        jTextFieldAddressNumber.setText("Number");
        jTextFieldAddressNumber.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                jTextFieldAddressNumberFocusGained(evt);
            }
        });

        jTextFieldStreet.setText("Street");
        jTextFieldStreet.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                jTextFieldStreetFocusGained(evt);
            }
        });
        jTextFieldStreet.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextFieldStreetActionPerformed(evt);
            }
        });

        jTextFieldPostCode.setText("PostCode");
        jTextFieldPostCode.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                jTextFieldPostCodeFocusGained(evt);
            }
        });

        jTextFieldSuburb.setText("Suburb");
        jTextFieldSuburb.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                jTextFieldSuburbFocusGained(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jTextFieldAddressNumber, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jTextFieldStreet))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                        .addComponent(jTextFieldSuburb)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jTextFieldPostCode, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTextFieldAddressNumber, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextFieldStreet, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTextFieldPostCode, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextFieldSuburb, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(15, Short.MAX_VALUE))
        );

        jButtonSubmit.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jButtonSubmit.setText("Submit");
        jButtonSubmit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonSubmitActionPerformed(evt);
            }
        });

        jPanel_Job_Types_Holder.setBackground(new java.awt.Color(119, 158, 203));
        jPanel_Job_Types_Holder.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Sekect Job Type(s)", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 11))); // NOI18N

        jCheckBoxGeneralPestControl.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jCheckBoxGeneralPestControl.setText("General Pest Control");
        jCheckBoxGeneralPestControl.setToolTipText("");

        jCheckBoxTermiteInspection.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jCheckBoxTermiteInspection.setText("Termite Inspection");
        jCheckBoxTermiteInspection.setToolTipText("");
        jCheckBoxTermiteInspection.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckBoxTermiteInspectionActionPerformed(evt);
            }
        });

        jCheckBoxTermiteControlFill.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jCheckBoxTermiteControlFill.setText("Termite control fill");

        jSpinner_Rooms.setToolTipText("Do not include Kitchen's and bathrooms here.");

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel3.setText("Kitchens/ Bathrooms:");

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel4.setText("Halls:");

        jLabel5.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel5.setText("Rooms:");

        jLabel6.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel6.setText("$10 per room.");

        jLabel7.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel7.setText("$15 per hall.");

        jLabel8.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel8.setText("Non-Charge Rooms.");

        javax.swing.GroupLayout jPanel_Job_Types_HolderLayout = new javax.swing.GroupLayout(jPanel_Job_Types_Holder);
        jPanel_Job_Types_Holder.setLayout(jPanel_Job_Types_HolderLayout);
        jPanel_Job_Types_HolderLayout.setHorizontalGroup(
            jPanel_Job_Types_HolderLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel_Job_Types_HolderLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel_Job_Types_HolderLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel_Job_Types_HolderLayout.createSequentialGroup()
                        .addGroup(jPanel_Job_Types_HolderLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jCheckBoxGeneralPestControl)
                            .addComponent(jCheckBoxTermiteInspection))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(jPanel_Job_Types_HolderLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel4, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel5, javax.swing.GroupLayout.Alignment.TRAILING)))
                    .addGroup(jPanel_Job_Types_HolderLayout.createSequentialGroup()
                        .addComponent(jCheckBoxTermiteControlFill)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel3)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel_Job_Types_HolderLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jSpinner_Halls, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jSpinner_Rooms, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jSpinner_Non_Charge_Rooms, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel_Job_Types_HolderLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel7)
                    .addComponent(jLabel8)
                    .addComponent(jLabel6))
                .addContainerGap())
        );
        jPanel_Job_Types_HolderLayout.setVerticalGroup(
            jPanel_Job_Types_HolderLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel_Job_Types_HolderLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel_Job_Types_HolderLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jCheckBoxGeneralPestControl)
                    .addComponent(jSpinner_Rooms, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5)
                    .addComponent(jLabel6))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel_Job_Types_HolderLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jCheckBoxTermiteInspection)
                    .addComponent(jSpinner_Halls, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4)
                    .addComponent(jLabel7))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel_Job_Types_HolderLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jCheckBoxTermiteControlFill)
                    .addComponent(jSpinner_Non_Charge_Rooms, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3)
                    .addComponent(jLabel8))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel_Contractor_Holder.setBackground(new java.awt.Color(119, 158, 203));
        jPanel_Contractor_Holder.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Select Contractor", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 11))); // NOI18N

        buttonGroupSelectContractor.add(jRadioButtonContractor1);
        jRadioButtonContractor1.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jRadioButtonContractor1.setText("Contractor 1");

        buttonGroupSelectContractor.add(jRadioButtonContractor2);
        jRadioButtonContractor2.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jRadioButtonContractor2.setText("Contractor 2");

        buttonGroupSelectContractor.add(jRadioButtonContractor3);
        jRadioButtonContractor3.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jRadioButtonContractor3.setText("Contractor 3");

        javax.swing.GroupLayout jPanel_Contractor_HolderLayout = new javax.swing.GroupLayout(jPanel_Contractor_Holder);
        jPanel_Contractor_Holder.setLayout(jPanel_Contractor_HolderLayout);
        jPanel_Contractor_HolderLayout.setHorizontalGroup(
            jPanel_Contractor_HolderLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel_Contractor_HolderLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jRadioButtonContractor1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jRadioButtonContractor2)
                .addGap(92, 92, 92)
                .addComponent(jRadioButtonContractor3)
                .addContainerGap())
        );
        jPanel_Contractor_HolderLayout.setVerticalGroup(
            jPanel_Contractor_HolderLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel_Contractor_HolderLayout.createSequentialGroup()
                .addGap(17, 17, 17)
                .addGroup(jPanel_Contractor_HolderLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jRadioButtonContractor1)
                    .addComponent(jRadioButtonContractor2)
                    .addComponent(jRadioButtonContractor3))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jButtonClear.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jButtonClear.setText("Clear");
        jButtonClear.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonClearActionPerformed(evt);
            }
        });

        jButtonClear1.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jButtonClear1.setText("Back");
        jButtonClear1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonClear1ActionPerformed(evt);
            }
        });

        jPanel_TableHolder.setBackground(new java.awt.Color(119, 158, 203));
        jPanel_TableHolder.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Exports Pending", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 11))); // NOI18N

        jTable_Alll_Job_Details.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Job ID", "Date", "Addres", "Jobs", "Contractor", "Cost"
            }
        ));
        jScrollPane2.setViewportView(jTable_Alll_Job_Details);

        javax.swing.GroupLayout jPanel_TableHolderLayout = new javax.swing.GroupLayout(jPanel_TableHolder);
        jPanel_TableHolder.setLayout(jPanel_TableHolderLayout);
        jPanel_TableHolderLayout.setHorizontalGroup(
            jPanel_TableHolderLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel_TableHolderLayout.createSequentialGroup()
                .addComponent(jScrollPane2)
                .addContainerGap())
        );
        jPanel_TableHolderLayout.setVerticalGroup(
            jPanel_TableHolderLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel_TableHolderLayout.createSequentialGroup()
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 87, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(6, 6, 6))
        );

        jButton_Clear_Table.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jButton_Clear_Table.setText("Clear Table");
        jButton_Clear_Table.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton_Clear_TableActionPerformed(evt);
            }
        });

        jButton_Load_Past_Jobs.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jButton_Load_Past_Jobs.setText("View All Jobs");
        jButton_Load_Past_Jobs.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton_Load_Past_JobsActionPerformed(evt);
            }
        });

        jButton1.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jButton1.setText("Export");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanelJobIdAndDate, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel_Job_Types_Holder, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel_Contractor_Holder, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jLabel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel_TableHolder, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jButtonClear1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButtonClear)
                .addGap(2, 2, 2)
                .addComponent(jButton_Clear_Table)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton_Load_Past_Jobs)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButtonSubmit, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanelJobIdAndDate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel_Job_Types_Holder, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel_Contractor_Holder, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel_TableHolder, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButtonClear1)
                    .addComponent(jButtonClear)
                    .addComponent(jButton_Clear_Table)
                    .addComponent(jButton_Load_Past_Jobs)
                    .addComponent(jButton1)
                    .addComponent(jButtonSubmit))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void jTextFieldJobIDFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTextFieldJobIDFocusGained
     //jTextFieldJobID.setText("");
        //Not sure why selecting a date will cause JobID to regain focus and reset.
    }//GEN-LAST:event_jTextFieldJobIDFocusGained

    private void jButtonSubmitActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonSubmitActionPerformed
      
    ///////////////// Protecting against Job Id Error ///////////////////   
        String jobID;
        /*
        * Using an infinite loop to force the correct data to be entered by
        * the user
        */
       
        while(true)
        {
            try
            {                       
                while(true)
                {
                    jobID = jTextFieldJobID.getText();
                    //Protecting against leaving the text as Number
                    if (jobID.equals("Number"))
                    {
                        jobID = JOptionPane.showInputDialog(this,
                                   "Error - Job Id cannot be Number",
                                   "Input a job ID",
                                   JOptionPane.ERROR_MESSAGE);
                        jTextFieldJobID.setText(jobID);
                        continue;
                     }else
                        {
                            break;
                        }
                }
                while(true)
                {   // protecting against leaving the text as blank.
                    if(jobID.equals(""))
                    {
                        jobID = JOptionPane.showInputDialog(this,
                            "Error - Job Id cannot be blank",
                            "Input a job ID",
                            JOptionPane.ERROR_MESSAGE);
                        jTextFieldJobID.setText(jobID);
                        continue;
                    }else
                    {
                       break;
                    }
                } // the end of the second while loop
                break;
            } // the end of the try
            catch(Exception e)
            {
                JOptionPane.showMessageDialog(this,
                        "Job ID Enty Error",
                        "ID Error",
                        JOptionPane.ERROR_MESSAGE);
            }                   
        }    
        //Storing the Job id
        JobID theJobID = new JobID(jobID);
       
        // Storing the date            
        
        try{
            /**
             * Reformatting the date so that it is easier to work with.
             */
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        stringDate = sdf.format(jDateChooser_Job_Date.getDate());
        dateManage = new DateManage(stringDate);
        }catch(Exception ex){
            JOptionPane.showMessageDialog(this,
                    "You have not stored a date",
                    "Date Error",
                    JOptionPane.ERROR_MESSAGE);
        }
        ///////////////// Now we are working with the address //////////////
        // Pulling the address from the text areas, settning error messages if the
        //User does not enter an address.
        String addressNumber = jTextFieldAddressNumber.getText();
        if(addressNumber.equals(""))
            {
                JOptionPane.showMessageDialog(this,
                        "No Street Number Entered",
                        "Error Street Number",
                        JOptionPane.ERROR_MESSAGE);
             }
        if(addressNumber.equals("Number"))
            {
                JOptionPane.showMessageDialog(this,
                        "No Street Number Entered",
                        "Error Street Number",
                        JOptionPane.ERROR_MESSAGE); 
            }   
        
        String addressStreet = jTextFieldStreet.getText();
        if(addressStreet.equals(""))
             {
                JOptionPane.showMessageDialog(this,
                    "No Street Entered",
                    "Error Street",
                    JOptionPane.ERROR_MESSAGE);
             }
        if(addressStreet.equals("Street"))
            {
                JOptionPane.showMessageDialog(this,
                    "No Street Entered",
                    "Error Street",
                    JOptionPane.ERROR_MESSAGE); 
             }  
        
        String addressSuburb = jTextFieldSuburb.getText();
        if(addressSuburb.equals(""))
             {
                JOptionPane.showMessageDialog(this,
                    "No Suburb Entered",
                    "Error Suburb",
                    JOptionPane.ERROR_MESSAGE);
             }
        if(addressSuburb.equals("Suburb"))
            {
                JOptionPane.showMessageDialog(this,
                    "No Suburb Entered",
                    "Error Suburb",
                    JOptionPane.ERROR_MESSAGE); 
            } 
        
        String addressPostCode = jTextFieldPostCode.getText();
        if(addressPostCode.equals(""))
             {
                JOptionPane.showMessageDialog(this,
                    "No PostCode Entered",
                    "Error PostCode",
                    JOptionPane.ERROR_MESSAGE);
             }
        if(addressPostCode.equals("PostCode"))
            {
                JOptionPane.showMessageDialog(this,
                    "No PostCode Entered",
                    "Error PostCode",
                    JOptionPane.ERROR_MESSAGE); 
            } 
        
        //Saving the address info inside the address class/object come runtime 
        Address address = new Address(addressNumber, addressStreet, 
                addressSuburb, addressPostCode);
         
    /////////////////////// We are now selecting the contractor ///////////////
    
       String contractor = ""; // setting the default contractor to blank.
        //It works by checking which radio button is selected
        // and depending on the radio button pressed, set the contractor string
        // as the text associated with it's radio button.
        if(jRadioButtonContractor1.isSelected())
        {
            contractor = jRadioButtonContractor1.getText();
        }else if(jRadioButtonContractor2.isSelected())
        {
            contractor = jRadioButtonContractor2.getText();
        }else if(jRadioButtonContractor3.isSelected())
        {
            contractor = jRadioButtonContractor3.getText();
        }
        
        // Creating an error message to be displayed if no contractor radio button has been selected.
        if(!jRadioButtonContractor1.isSelected()&&!jRadioButtonContractor2.isSelected()&&!jRadioButtonContractor3.isSelected())
        {
            JOptionPane.showMessageDialog(this,
                    "No Contractor has been selected",
                    "Select a contractor",
                    JOptionPane.ERROR_MESSAGE);
            contractor = "No Contractor selected";
        }
        
       ///////////// Now getting the cost value from the check box's //////////
        double cost = 0; 
        /*double cost will be updated once the program goes through the if 
        *statements.
        */
        
        /*These strings will be updated by the if statements if they are 
        *selected, if not they will remain blank and be displayed in the
        *jTextFieldJobCard as blank.
        */
        
        /**
         * rooms and halls will take the values from the spinners and then times
         * the values by either 10 for rooms or 15 for halls.
         */
        int rooms = (int) jSpinner_Rooms.getValue();
        int halls = (int) jSpinner_Halls.getValue();
      
        double totalRoomCost = rooms * ROOM_COST;
        //The next two println were used to check during testing.
       // System.out.println(totalRoomCost);
        double totalHallCost = halls * HALL_COST;
       // System.out.println(totalHallCost);
        
        cost += totalRoomCost;
        cost += totalHallCost;
        String generalPestControl = "";
        String termiteInspectCost = "";
        String termiteFillCost = "";
        if(jCheckBoxGeneralPestControl.isSelected())
        {
            cost += GENERAL_PEST_COST;
            generalPestControl = "General Pest Control";
        }
        if(jCheckBoxTermiteInspection.isSelected())
        {
            cost += TERMITE_INSPECT_COST;
            termiteInspectCost = "Termite Inspection";
        }
        if(jCheckBoxTermiteControlFill.isSelected())
        {
            if(jCheckBoxTermiteInspection.isSelected())
            {
                cost += TERMITE_FILL_COST;
                termiteInspectCost = "Termite Inspection";
                termiteFillCost = "Termite Control Fill";
            }else
            {
            cost += TERMITE_FILL_COST+TERMITE_INSPECT_COST;
            termiteFillCost = "Termite Control Fill";
            }
        }
        // Below checks to see if a job is selected. If not, a message will pop up to inform the user.
        if(!jCheckBoxGeneralPestControl.isSelected() && ! jCheckBoxTermiteInspection.isSelected() && ! jCheckBoxTermiteControlFill.isSelected())
        {
            JOptionPane.showMessageDialog(this,
                    "No job selected",
                    "Select a job",
                    JOptionPane.ERROR_MESSAGE);
        }
        
        try{
        //Storing to a string for convenience when working with text areas
        String costToString = String.valueOf(cost);
        
        // Adding the cost to the cost object come run time.
        Cost theCost = new Cost(costToString);
        
        /*
        * Creating a job object come run time which has a job id, an address, a 
        * date, a contractor and a cost
        */
        Job theJob = new Job(theJobID.getJobID(),address,dateManage,contractor,
                theCost);
        
        // Writing to a text file


        /*
        * Displaying all theJob details onto a text area, by calling the 
        * classes and their get methods.
        */
      
            DefaultTableModel model = (DefaultTableModel) jTable_Alll_Job_Details.getModel();
            model.addRow(new Object[]{theJob.getJobid(),theJob.getDateManage().getDate(),
            theJob.getAddress().getNumber()+ " " + theJob.getAddress().getStreet()+ " " + theJob.getAddress().getSuburb()+ " " + theJob.getAddress().getPostCode(),
            theJob.getContractor(), generalPestControl+ " " + termiteInspectCost + " "+ termiteFillCost, theJob.getTheCost().getCost()});
            
        
        }catch(Exception e)
        {
            JOptionPane.showMessageDialog(this, 
                    "Unknown Error",
                    "Error",
                    JOptionPane.ERROR_MESSAGE);
            System.out.println("There is an error, debug.");
        }
        /*
        * I made it so you can not focus on the jTextAreaJobCard as I do not 
        * want the user to interact with it.
        */  
        
           //Below is the end of this button's event 
    }//GEN-LAST:event_jButtonSubmitActionPerformed

    private void jButtonClearActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonClearActionPerformed
        // This button is used to clear all field and return them to default.
        jTextFieldJobID.setText("Number");
        jDateChooser_Job_Date.setCalendar(null);
        jTextFieldAddressNumber.setText("Number");
        jTextFieldStreet.setText("Street");
        jTextFieldSuburb.setText("Suburb");
        jTextFieldPostCode.setText("PostCode");       

    }//GEN-LAST:event_jButtonClearActionPerformed

    private void jTextFieldAddressNumberFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTextFieldAddressNumberFocusGained
        //When you click on the text field, the current text will be cleared.
        jTextFieldAddressNumber.setText("");
    }//GEN-LAST:event_jTextFieldAddressNumberFocusGained

    private void jTextFieldStreetFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTextFieldStreetFocusGained
        //When you click on the text field, the current text will be cleared.
        jTextFieldStreet.setText("");
    }//GEN-LAST:event_jTextFieldStreetFocusGained

    private void jTextFieldSuburbFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTextFieldSuburbFocusGained
        //When you click on the text field, the current text will be cleared.
        jTextFieldSuburb.setText("");
    }//GEN-LAST:event_jTextFieldSuburbFocusGained

    private void jTextFieldPostCodeFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTextFieldPostCodeFocusGained
        //When you click on the text field, the current text will be cleared.
        jTextFieldPostCode.setText("");
    }//GEN-LAST:event_jTextFieldPostCodeFocusGained

    private void jButtonClear1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonClear1ActionPerformed
        this.dispose();
        frameManager.loadTitlePage();
    }//GEN-LAST:event_jButtonClear1ActionPerformed

    private void jTextFieldStreetActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextFieldStreetActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextFieldStreetActionPerformed

    private void jCheckBoxTermiteInspectionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckBoxTermiteInspectionActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jCheckBoxTermiteInspectionActionPerformed

    private void jButton_Clear_TableActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton_Clear_TableActionPerformed
        DefaultTableModel dm = (DefaultTableModel) jTable_Alll_Job_Details.getModel();
        int rowCount = dm.getRowCount();
        //Remove rows one by one from the end of the table
        for (int i = rowCount - 1; i >= 0; i--) 
        {
            dm.removeRow(i);
        }
    }//GEN-LAST:event_jButton_Clear_TableActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
       
       try
       { 
           // Creating the Pest.text file
           File file = new File("Pest.txt");
           //Checking if the file exists or not
           if(!file.exists())
            {
                file.createNewFile();
            }
           /**
            * Creating a new file writer, using true so it does not over write 
            * the current contents on the file.
            */
            FileWriter fw = new FileWriter(file.getAbsoluteFile(),true);
            BufferedWriter bw = new BufferedWriter(fw);
            /*
            *Nexted for loop to read form right to left, up and down through
            * the jtable and write it to the text file/
            */
            for(int i = 0; i < jTable_Alll_Job_Details.getRowCount(); i++)
            {
                for(int j = 0; j < jTable_Alll_Job_Details.getColumnCount(); j++)
                {
                    /**
                     * The ":" is used so I can split up the text when I want to
                     * read it back into the table
                     */
                    bw.write((String)jTable_Alll_Job_Details.getModel().getValueAt(i, j) + ":");
                }
                bw.write((String) "\n");
            }
            bw.close();
            fw.close();
            JOptionPane.showMessageDialog(this,
                    "Table Exported to text file",
                    "Export Successful",
                    JOptionPane.PLAIN_MESSAGE);
       }catch(Exception ex)
       {
           JOptionPane.showMessageDialog(this,
                   "File can not be created",
                   "Error File creation",
                   JOptionPane.ERROR_MESSAGE);
       }
        DefaultTableModel dm = (DefaultTableModel) jTable_Alll_Job_Details.getModel();
        int rowCount = dm.getRowCount();
        //Remove rows one by one from the end of the table
        for (int i = rowCount - 1; i >= 0; i--) 
        {
            dm.removeRow(i);
        }
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton_Load_Past_JobsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton_Load_Past_JobsActionPerformed
        frameManager.loadPastJobs();
       this.dispose();
    }//GEN-LAST:event_jButton_Load_Past_JobsActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(DataEntryForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(DataEntryForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(DataEntryForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(DataEntryForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new DataEntryForm().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup buttonGroupSelectContractor;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButtonClear;
    private javax.swing.JButton jButtonClear1;
    private javax.swing.JButton jButtonSubmit;
    private javax.swing.JButton jButton_Clear_Table;
    private javax.swing.JButton jButton_Load_Past_Jobs;
    private javax.swing.JCheckBox jCheckBoxGeneralPestControl;
    private javax.swing.JCheckBox jCheckBoxTermiteControlFill;
    private javax.swing.JCheckBox jCheckBoxTermiteInspection;
    private com.toedter.calendar.JDateChooser jDateChooser_Job_Date;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabelDate;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanelJobIdAndDate;
    private javax.swing.JPanel jPanel_Contractor_Holder;
    private javax.swing.JPanel jPanel_Job_Types_Holder;
    private javax.swing.JPanel jPanel_TableHolder;
    private javax.swing.JRadioButton jRadioButtonContractor1;
    private javax.swing.JRadioButton jRadioButtonContractor2;
    private javax.swing.JRadioButton jRadioButtonContractor3;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JSpinner jSpinner_Halls;
    private javax.swing.JSpinner jSpinner_Non_Charge_Rooms;
    private javax.swing.JSpinner jSpinner_Rooms;
    private javax.swing.JTable jTable_Alll_Job_Details;
    private javax.swing.JTextField jTextFieldAddressNumber;
    private javax.swing.JTextField jTextFieldJobID;
    private javax.swing.JTextField jTextFieldPostCode;
    private javax.swing.JTextField jTextFieldStreet;
    private javax.swing.JTextField jTextFieldSuburb;
    // End of variables declaration//GEN-END:variables
}
