
package CQUPControl;

import java.io.Serializable;
import java.util.Date;

/** 
 * Programmer: Mitchell Boland / S0249789
 * File: Job.java
 * Date: 06/04/2016
 * Purpose: COIT11134 Object Oriented Programming (T1,2016)
 * The Job class is a class which takes the information from all the other
 * current classes. It has a jobID, address, dateManager, contractor and cost.
 * Come the submit button being pressed on the DataEntryForm, the Job class 
 * swings into action and displays all required information in a text area.
 */
public class Job implements Serializable
{
    
    private String jobid;
    private Address address;
    private String contractor;
    private Cost theCost;
    private DateManage dateManage;

    public Job(String jobid, Address address, DateManage dateManage, String contractor, Cost theCost)
    {
        this.jobid = jobid;
        this.address = address;
        this.dateManage = dateManage;
        this.contractor = contractor;
        this.theCost = theCost;
    }

    Job(String jobid, String address, Date newDate, String contractor, String cost) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    public DateManage getDateManage() {
        return dateManage;
    }

    public void setDateManage(DateManage dateManage) {
        this.dateManage = dateManage;
    }
    
    public Cost getTheCost() {
        return theCost;
    }

    public void setTheCost(Cost theCost) {
        this.theCost = theCost;
    }
    
    public String getContractor() {
        return contractor;
    }

    public void setContractor(String contractor) {
        this.contractor = contractor;
    } 

    public Address getAddress() 
    {
        return address;
    }

    public void setAddress(Address address) 
    {
        this.address = address;
    }
    
    public String getJobid() 
    {
        return jobid;
    }

    public void setJobid(String jobid) 
    {
        this.jobid = jobid;
    }    
}
