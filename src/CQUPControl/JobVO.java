package CQUPControl;
/**
 * The purpose of the JobVo class is to be used to create an array list containing
 * all the aspects of a job. The date detail is then compared and the most recent
 * date will appear at the top of the jTable on the title page.
 */
import java.util.Date;

public class JobVO {

	
   private String line;
   private String[] details;
   private String jobid;
   private Date date;
   private String address;
   private String contractor;
   private String jobs;
   private String cost;
    
    public JobVO(String line, String[] details, String jobid, Date date,
			String address, String contractor, String jobs, String cost) {
		super();
		this.line = line;
		this.details = details;
		this.jobid = jobid;
		this.date = date;
		this.address = address;
		this.contractor = contractor;
		this.jobs = jobs;
		this.cost = cost;
	}
    
    
	/**
	 * @return the line
	 */
	public String getLine() {
		return line;
	}
	/**
	 * @param line the line to set
	 */
	public void setLine(String line) {
		this.line = line;
	}
	/**
	 * @return the details
	 */
	public String[] getDetails() {
		return details;
	}
	/**
	 * @param details the details to set
	 */
	public void setDetails(String[] details) {
		this.details = details;
	}
	/**
	 * @return the jobid
	 */
	public String getJobid() {
		return jobid;
	}
	/**
	 * @param jobid the jobid to set
	 */
	public void setJobid(String jobid) {
		this.jobid = jobid;
	}
	/**
	 * @return the date
	 */
	public Date getDate() {
		return date;
	}
	/**
	 * @param date the date to set
	 */
	public void setDate(Date date) {
		this.date = date;
	}
	/**
	 * @return the address
	 */
	public String getAddress() {
		return address;
	}
	/**
	 * @param address the address to set
	 */
	public void setAddress(String address) {
		this.address = address;
	}
	/**
	 * @return the contractor
	 */
	public String getContractor() {
		return contractor;
	}
	/**
	 * @param contractor the contractor to set
	 */
	public void setContractor(String contractor) {
		this.contractor = contractor;
	}
	/**
	 * @return the jobs
	 */
	public String getJobs() {
		return jobs;
	}
	/**
	 * @param jobs the jobs to set
	 */
	public void setJobs(String jobs) {
		this.jobs = jobs;
	}
	/**
	 * @return the cost
	 */
	public String getCost() {
		return cost;
	}
	/**
	 * @param cost the cost to set
	 */
	public void setCost(String cost) {
		this.cost = cost;
	}
        
}
