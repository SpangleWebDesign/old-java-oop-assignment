
package CQUPControl;
/** 
 * Programmer: Mitchell Boland / S0249789
 * File: JobID.java
 * Date: 06/04/2016
 * Purpose: COIT11134 Object Oriented Programming (T1,2016)
 * The JobID class is used to store and give the jobID to the Job class.
 */
public class JobID 
{
    private String jobID;

    public JobID(String jobID)
    {
        this.jobID = jobID;
    }

    public String getJobID() 
    {
        return jobID;
    }

    public void setJobID(String jobID) 
    {
        this.jobID = jobID;
    }    
}
