
package CQUPControl;
/** 
 * Programmer: Mitchell Boland / S0249789
 * File: Address.java
 * Date: 06/04/2016
 * Purpose: COIT11134 Object Oriented Programming (T1,2016)
 * The Address class is used to store the street number, name, suburb and
 * postCode. The info here is then stored in a Job Object and its get methods
 * will be called.
 */
public class Address 
{
    
    private String number;
    private String street;
    private String suburb;
    private String postCode;
    
     public Address(String number, String street, String suburb, 
             String postCode)
    {
        this.number = number;
        this.street = street;
        this.suburb = suburb;
        this.postCode = postCode;
    }

    public String getNumber() 
    {
        return number;
    }

    public String getStreet() 
    {
        return street;
    }

    public String getSuburb() 
    {
        return suburb;
    }
    
    public String getPostCode() 
    {
        return postCode;
    }
}
