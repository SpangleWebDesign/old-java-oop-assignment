

package CQUPControl;

import javax.swing.JFrame;

/** 
 * Programmer: Mitchell Boland / S0249789
 * File: FrameManager.java
 * Date: 06/04/2016
 * Purpose: COIT11134 Object Oriented Programming (T1,2016)
 * The FrameManager class is being created to load and set frames to be visible
 * When a button requesting to load a new frame is pressed, methods in this 
 * class will be called.
 */

public class FrameManager extends JFrame
{
    
    //Method to be called when the user wishes to close the application
    public void ExitApplication()
    {
        System.exit(0);
    }
    
    //Metod called when the user wishes to load the DataEntryForm
    public void loadDataEntryForm()
    {
        DataEntryForm dataEntryForm = new DataEntryForm();
        dataEntryForm.setVisible(true);
        dataEntryForm.setLocationRelativeTo(this);
    }
    
    //Method called when the user wishes to load the TitlePage
    public void loadTitlePage()
    {
        TitlePage titlePage = new TitlePage();
        titlePage.setVisible(true);
    } 
    
    public void loadPastJobs()
    {
        PastJobs pj = new PastJobs();
        pj.setVisible(true);
    }
 
}
