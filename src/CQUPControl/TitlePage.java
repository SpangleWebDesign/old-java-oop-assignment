
package CQUPControl;

import java.util.TreeMap;
import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;
import java.util.Scanner;
import java.util.SortedMap;
import javax.swing.JOptionPane;
import javax.swing.RowFilter;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;
/** 
 * Programmer: Mitchell Boland / S0249789
 * File: Address.java
 * Date: 06/04/2016
 * Purpose: COIT11134 Object Oriented Programming (T1,2016)
 * The TitlePage extends a JFrame. It is used as a title page in which the user
 * can begin to enter jobs, or exit the application.
 */
public class TitlePage extends javax.swing.JFrame {


    //Frame manager will be used to move from one frame to another.
    FrameManager frameManager = new FrameManager();
    
    TreeMap<String, JobVO> previousJobs = new TreeMap<String, JobVO>();
    
    private double contractorOne = 0;
    private double contractorTwo = 0;
    private double contractorThree = 0;
    
    public TitlePage() 
    {
        initComponents();
        setResizable(false);
        readFromText();
        sortTheTableByDate();
        
    }

    //Filter JTable
    
    private void filter(String query)
    {
        DefaultTableModel model = (DefaultTableModel) jTable_All_Jobs.getModel();
        TableRowSorter<DefaultTableModel> trs = new TableRowSorter<DefaultTableModel>(model);
        jTable_All_Jobs.setRowSorter(trs);
        
        trs.setRowFilter(RowFilter.regexFilter(query));
        
    }
    
    private void readFromText()
    {
         try
        {
            
            File f = new File("Pest.txt");
            Scanner sc = new Scanner(f);
            DefaultTableModel model = (DefaultTableModel) jTable_All_Jobs.getModel();
            model.setRowCount(0);
            
            while(sc.hasNextLine())
            {
                String line = sc.nextLine();
                String[] details = line.split(":");
                String jobid = details[0];
                String date = details[1];
                String address = details[2];
                String contractor = details[3];
                String jobs = details[4];
                String cost = details[5];                          
                model.addRow(new String[]{jobid, date, address, contractor, jobs,cost});
                
                Date dateA=null;
                String pattern = "dd/MM/yyyy";
                SimpleDateFormat format = new SimpleDateFormat(pattern);
                try {
                  dateA = format.parse(date);
                } catch (Exception e) {
                  e.printStackTrace();
                }
                
                JobVO jobvo = new JobVO(line, details, jobid, dateA, address, contractor, jobs, cost);
                previousJobs.put(jobid, jobvo);
                
                if (contractor.equals("Contractor 1"))
                {
                    contractorOne += Double.parseDouble(cost);
                }
                if (contractor.equals("Contractor 2"))
                {
                    contractorTwo += Double.parseDouble(cost);
                }
                if (contractor.equals("Contractor 3"))
                {
                    contractorThree += Double.parseDouble(cost);
                }
               // jTextField_Con1.setText(String.valueOf(contractorOne));
               // jTextField_Con2.setText(String.valueOf(contractorTwo));
               // jTextField_Con3.setText(String.valueOf(contractorThree));
                
               // Job(String jobid, Address address, DateManage dateManage, String contractor, Cost theCost)
               //list.add( new Matrices(1,1,10) );
            }
            
            /*Iterator it = sortedMap.entrySet().iterator();
            while (it.hasNext()) {
                Map.Entry pair = (Map.Entry)it.next();
                
                JobVO job = (JobVO) pair.getValue();
                System.out.println(pair.getKey() + " = " + job.getDate());
                
            }*/
            
            
            System.out.println("Hi");
            
        }catch(Exception ex)
        {
            JOptionPane.showMessageDialog(this,
                    "There was an error reading the file",
                    "Error with reader",
                    JOptionPane.ERROR_MESSAGE);
        }
        
    }
    
    private void sortTheTableByDate()
    {
        	  System.out.println("eee");
                
                //Clear table and refresh
                DefaultTableModel model = (DefaultTableModel) jTable_All_Jobs.getModel();
                model.setRowCount(0);
                
                SortedMap<String, JobVO> sortedMap = new TreeMap<String, JobVO>(new SortMapComparator(previousJobs));
                sortedMap.putAll(previousJobs);
                
                Iterator it = sortedMap.entrySet().iterator();
                while (it.hasNext()) {
                    Map.Entry pair = (Map.Entry)it.next();
                    
                    JobVO job = (JobVO) pair.getValue();
                    System.out.println(pair.getKey() + " = " + job.getDate());
                    
                    DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
                    
                    //to convert Date to String, use format method of SimpleDateFormat class.
                    String strDate = dateFormat.format(job.getDate());
                    
                    model.addRow(new String[]{job.getJobid(), strDate, job.getAddress(),
                    		job.getContractor(), job.getJobs(),job.getCost()});
                 
                }
    }
    /**
     * This method is called from within the constructor to initialize the form.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanelTitlePage = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabelStudentNumber = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jButton_Past_Jobs = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable_All_Jobs = new javax.swing.JTable();
        jButton3 = new javax.swing.JButton();
        jTextField_Contractor_Net_Profit = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        jTextField_Filter_Text = new javax.swing.JTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Title page");

        jPanelTitlePage.setBackground(new java.awt.Color(119, 158, 203));
        jPanelTitlePage.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED, java.awt.Color.black, new java.awt.Color(0, 0, 0), java.awt.Color.black, java.awt.Color.black));

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("CQU Pest Control");

        jLabelStudentNumber.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabelStudentNumber.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabelStudentNumber.setText("By S0249789");

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel2.setText("Mitchell Boland");

        jButton1.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jButton1.setText("Begin");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jButton2.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jButton2.setText("Exit");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jButton_Past_Jobs.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jButton_Past_Jobs.setText("Jobs");
        jButton_Past_Jobs.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton_Past_JobsActionPerformed(evt);
            }
        });

        jTable_All_Jobs.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Job ID", "Job Date", "Address", "Contractor", "Jobs Conducted", "Cost"
            }
        ));
        jScrollPane1.setViewportView(jTable_All_Jobs);

        jButton3.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jButton3.setText("Check Contractor Net Profit");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel3.setText("Search Table:");

        jTextField_Filter_Text.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField_Filter_TextActionPerformed(evt);
            }
        });
        jTextField_Filter_Text.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTextField_Filter_TextKeyReleased(evt);
            }
        });

        javax.swing.GroupLayout jPanelTitlePageLayout = new javax.swing.GroupLayout(jPanelTitlePage);
        jPanelTitlePage.setLayout(jPanelTitlePageLayout);
        jPanelTitlePageLayout.setHorizontalGroup(
            jPanelTitlePageLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jLabelStudentNumber, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanelTitlePageLayout.createSequentialGroup()
                .addGap(23, 23, 23)
                .addGroup(jPanelTitlePageLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanelTitlePageLayout.createSequentialGroup()
                        .addComponent(jButton3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jTextField_Contractor_Net_Profit, javax.swing.GroupLayout.PREFERRED_SIZE, 165, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jScrollPane1)
                    .addGroup(jPanelTitlePageLayout.createSequentialGroup()
                        .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jTextField_Filter_Text, javax.swing.GroupLayout.PREFERRED_SIZE, 175, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 79, Short.MAX_VALUE)
                        .addComponent(jButton_Past_Jobs, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        jPanelTitlePageLayout.setVerticalGroup(
            jPanelTitlePageLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelTitlePageLayout.createSequentialGroup()
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabelStudentNumber, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanelTitlePageLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton_Past_Jobs, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel3)
                    .addComponent(jTextField_Filter_Text, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 128, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(jPanelTitlePageLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton2)
                    .addComponent(jButton3)
                    .addComponent(jTextField_Contractor_Net_Profit, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanelTitlePage, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanelTitlePage, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        String contractorProfit = JOptionPane.showInputDialog(this,
            "Select Contractor: Contractor 1, Contractor 2, Contractor 3",
            "Enter Contractor",
            JOptionPane.PLAIN_MESSAGE);
        if(contractorProfit.equalsIgnoreCase("Contractor 1"))
        {
            jTextField_Contractor_Net_Profit.setText(String.valueOf(contractorOne));
        }
        if(contractorProfit.equalsIgnoreCase("Contractor 2"))
        {
            jTextField_Contractor_Net_Profit.setText(String.valueOf(contractorTwo));
        }
        if(contractorProfit.equalsIgnoreCase("Contractor 3"))
        {
            jTextField_Contractor_Net_Profit.setText(String.valueOf(contractorThree));
        }
        if(!contractorProfit.equalsIgnoreCase("Contractor 1") && ! contractorProfit.equalsIgnoreCase("Contractor 2")
            && !contractorProfit.equalsIgnoreCase("Contractor 3")){
            JOptionPane.showMessageDialog(this,
                "Error, Contractor does not exist",
                "Error Selecting Contractor",
                JOptionPane.ERROR_MESSAGE);
            jTextField_Contractor_Net_Profit.setText("Error");
        }

    }//GEN-LAST:event_jButton3ActionPerformed

    private void jButton_Past_JobsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton_Past_JobsActionPerformed
        frameManager.loadPastJobs();
        this.dispose();
    }//GEN-LAST:event_jButton_Past_JobsActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        frameManager.ExitApplication(); // exits the program.
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        frameManager.loadDataEntryForm();
        
        this.dispose();
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jTextField_Filter_TextKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField_Filter_TextKeyReleased
        String query = jTextField_Filter_Text.getText();
        filter(query);
    }//GEN-LAST:event_jTextField_Filter_TextKeyReleased

    private void jTextField_Filter_TextActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField_Filter_TextActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField_Filter_TextActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(TitlePage.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(TitlePage.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(TitlePage.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(TitlePage.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new TitlePage().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton_Past_Jobs;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabelStudentNumber;
    private javax.swing.JPanel jPanelTitlePage;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTable_All_Jobs;
    private javax.swing.JTextField jTextField_Contractor_Net_Profit;
    private javax.swing.JTextField jTextField_Filter_Text;
    // End of variables declaration//GEN-END:variables
}
