package CQUPControl;
/**
 * The purpose of the SortMapComparator is to implement a Comparator For the 
 * JobVO object and sort it by date.
 */
import java.util.Comparator;
import java.util.Date;
import java.util.Map;

public class SortMapComparator implements Comparator<String> {
	private Map<String, JobVO> sourceMap;

	public SortMapComparator(Map<String, JobVO> sourceMap) {
		this.sourceMap = sourceMap;
	}

	@Override
	public int compare(String key1, String key2) {
		
		JobVO job1 = sourceMap.get(key1);
		JobVO job2 = sourceMap.get(key2);

		Date o1CreationDate = job1.getDate();
		Date o2CreationDate = job2.getDate();
                // Below compares one date to the other
		return o2CreationDate.compareTo(o1CreationDate);
	}
}

