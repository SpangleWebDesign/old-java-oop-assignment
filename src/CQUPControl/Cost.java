
package CQUPControl;
/** 
 * Programmer: Mitchell Boland / S0249789
 * File: Cost.java
 * Date: 06/04/2016
 * Purpose: COIT11134 Object Oriented Programming (T1,2016)
 * The Cost class is a simple class used to take a cost and store it in the 
 * Job object. Its get method will be called.
 */
public class Cost 
{
    private String cost;
    
    public Cost(String cost)
    {
        this.cost = cost;
    }
    
    public String getCost() {
        return cost;
    }  
}
